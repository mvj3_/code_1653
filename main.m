//
//  main.m
//  Learning0510 KVC
//
//  Created by  apple on 13-5-10.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Car.h"
#import "Engine.h"
#import "Tire.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        Car *car = [[Car alloc] init];
        //使用KVC（键/值编码）
        [car setValue:@"Herbie" forKey:@"name"];
        [car setValue:@"Honda" forKey:@"make"];
        [car setValue:@"CRX" forKey:@"model"];
        
        //使用路径的时候，用.分隔不同属性名称，
        //如查询car的engine.horsepower， 可以写成[car valueForKeyPath:@"engine.horsepower"],
        //注意，path里面不需要再写car了
        
        [car setValue:[NSNumber numberWithInt:2] forKeyPath:@"numberOfDoors"];
        [car setValue:[NSNumber numberWithInt:1984] forKeyPath:@"modelYear"];
        [car setValue:[NSNumber numberWithFloat:110000] forKeyPath:@"mileage"];
        
        for(int i = 0 ; i < 4; i ++ ){
            Tire * tire = [[Tire alloc ] init ];
            [car setTire:tire atIndex:i];
            
            [tire release];
        }
        
        //=====    写法一    ========
//        Engine *engine = [[Engine alloc] init];
//        [engine setValue:[NSNumber numberWithInt:145] forKey:@"horsepower"];      
//        [car setValue:engine forKey:@"engine"];
        
        //=====    写法二    ========
        [car setValue:[NSNumber numberWithInt:145] forKeyPath:@"engine.horsepower"];
        
        NSLog(@"%@\n\n", car);
        
        //运算，除了tires.@count之外，
        //还有tires.@sum.pressure(计算没有意义，仅仅说明语法）
        //和tires.@avg.pressure
        //tires.@min.pressure
        //tires.@max.pressure
        //tires.@distinctUnionOfObjects.pressure等等
        NSNumber *count = [car valueForKeyPath: @"tires.@count"];
        NSLog(@"Car has %@ tires", count); //这里不需要用%d， 因为会自动装箱、拆箱
        
        [car release];
    }
    return 0;
}

