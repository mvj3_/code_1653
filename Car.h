//
//  Car.h
//  Learning0510 KVC
//
//  Created by  apple on 13-5-10.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <Foundation/Foundation.h>
@class  Engine;
@class Tire;
@interface Car : NSObject <NSCopying>
{
    NSString * name;
    NSMutableArray * tires;
    Engine * engine;
    
    NSString * make;
    NSString * model;
    int modelYear;
    int numberOfDoors;
    float mileage;
}

//使用KVC时，不需要定义setter、getter，可以直接获取实例变量

//@property (copy) NSString * name;
//@property (retain) Engine * engine;
//@property (copy) NSString * make;
//@property (copy) NSString * model;
//@property int modelYear;
//@property int numberOfDoors;
//@property float mileage;

-(void)setTire:(Tire *)tire atIndex:(int)index;
-(Tire *)tireAtIndex:(int)index;
@end
